
const http = require('http');
const crypto = require('crypto');
const https = require('https');

//const OCTO_WIZARD_HOOK_SECRET="mybaby"


const createComparisonSignature = (body) => {
  const hmac = crypto.createHmac('sha1', OCTO_WIZARD_HOOK_SECRET);
  const self_signature = hmac.update(JSON.stringify(body)).digest('hex');
  return `sha1=${self_signature}`; // shape in GitHub header
}

const compareSignatures = (signature, comparison_signature) => {
  const source = Buffer.from(signature);
  const comparison = Buffer.from(comparison_signature);
  return crypto.timingSafeEqual(source, comparison); // constant time comparison
}


http.createServer(function (req, res) {


    req.on('data', function(chunk) {

      //new Promise(function(resolve, reject){
        var signature = req.headers['x-op-signature'];
        //const comparison_signature = createComparisonSignature(chunk);

        //if (!compareSignatures(signature, comparison_signature)) {
        if ( ! signature ) { // we can just simply detect there's signature from op, rather than compare it in simplest tolerance

          console.log("Signature unmatched:"+signature);
          res.statusCode = 401;
          res.statusMessage = 'Mismatched signatures';
          return res;
        }

        var hookData = chunk.toString();
        console.log(hookData);

        // need to confirm it's from Open Project from the header, even we have a secret port,
        // since it could be polluted by other random calls from other computers or applications,
        // which will affect the following logic

        //const signature = req.headers.signature;
        //console.log(signature);
        console.log(req.headers)

        var obj = JSON.parse(hookData);
        var action = obj.action;
        var arrayAction = action.split(":");
        var payload = JSON.stringify({
            text: obj.work_package._embedded.type.name + "#"+obj.work_package.id+ " <https://pm.musicoin.org/projects/your-scrum-project/work_packages/"+obj.work_package.id +"|" + obj.work_package.subject+"> ("+ arrayAction[1]+")",
          });
          // We just need a minimalism text to notify relevant group
          // For more fancy cards can refer to https://developers.google.com/hangouts/chat/reference/message-formats/cards

          const options = {
            hostname: process.env.WEBHOOK_HOST,
            method: "POST",
            path: process.env.WEBHOOK_PATH,   // final room
            //path: "/v1/spaces/AAAA6WtkiLk/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=g54mD0T4vp06-L56o4OkOIWHHibdIF_mQl8Vqy6A2lQ%3D",   // test room
          };

          // launch the payload to Google Hangout, based on the webhook address generated in specific room
          const reqs = https.request(options,
              (ress) => ress.on("data", () => console.log("OK")))
          reqs.on("error", (error) => console.log(JSON.stringify(error)));
          reqs.write(payload);
          reqs.end();

      //});  //Promise

    });
    res.statusCode = 200;
    res.end();
}).listen(process.env.PORT);   // the service http://ipaddress_or_url:8080 should be added to Open Project as a new webhook
